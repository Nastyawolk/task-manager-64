package ru.volkova.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.volkova.tm")
public class ApplicationConfiguration {

}
