package ru.volkova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.model.Task;
import ru.volkova.tm.repository.ProjectRepository;
import ru.volkova.tm.repository.TaskRepository;

@Controller
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;
    
    @Autowired
    private ProjectRepository projectRepository;
    
    @GetMapping("/task/create")
    public String create() {
        taskRepository.save(new Task("Task" + System.currentTimeMillis()));
        return "redirect:/tasks";
    }
    
    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }
    
    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskRepository.save(task);
        return "redirect:/tasks";
    }
    
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
       final Task task = taskRepository.findById(id);
       ModelAndView modelAndView = new ModelAndView();
       modelAndView.setViewName("task-edit");
       modelAndView.addObject("task", task);
       modelAndView.addObject("projects", projectRepository.findAll());
       modelAndView.addObject("statuses", Status.values());
       return modelAndView;
    }
    
}
